import { Project } from "./modules/models/project";
import { GitHubService } from "./modules/services/github.service";
import { GitLabService } from "./modules/services/gitlab.service";

namespace main {
  const config = {
    github: {
      username: "{{ n2v:github.username }}",
      accessToken: "{{ n2v:github.accessToken }}",
    },

    gitlab: {
      username: "{{ n2v:gitlab.username }}",
      accessToken: "{{ n2v:gitlab.accessToken }}",
    },
  };

  const instances = {
    githubService: new GitHubService(
      config.github.username,
      config.github.accessToken
    ),

    gitlabService: new GitLabService(
      config.gitlab.username,
      config.gitlab.accessToken
    ),
  };

  function buildProjectItemEl(project: Project) {
    const projectItemEl = document.createElement("div");
    projectItemEl.className = "project-item";

    {
      {
        const nameEl = document.createElement("div");
        nameEl.className = "name";
        nameEl.textContent = project.name;
        nameEl.setAttribute(
          "data-first-letter",
          String(
            project.name.includes("/")
              ? project.name[project.name.indexOf("/") + 1]
              : project.name[0]
          ).toUpperCase()
        );
        projectItemEl.appendChild(nameEl);
      }

      {
        const descriptionEl = document.createElement("div");
        descriptionEl.className = "description";
        descriptionEl.textContent = project.description;
        projectItemEl.appendChild(descriptionEl);
      }

      {
        const languagesEl = document.createElement("div");
        languagesEl.className = "languages";

        project.languages.then((languages) => {
          for (const lang of languages) {
            const languageEl = document.createElement("div");
            languageEl.className = "language";
            languageEl.textContent = lang;
            languagesEl.appendChild(languageEl);
          }
        });
        projectItemEl.appendChild(languagesEl);
      }

      {
        /// REACH OUT
        const linkEl = document.createElement("a");
        linkEl.classList.add("link");
        linkEl.href = project.url;
        linkEl.target = "_blank";
        linkEl.textContent = "Goto";

        projectItemEl.appendChild(linkEl);
      }
    }

    return projectItemEl;
  }

  function buildGitLabProjects(pageIndex: number, pageSize: number) {
    const navBoxEl = document.querySelector(
      "#project #gitlab .nav-box"
    ) as HTMLElement;
    const loadingEl = document.querySelector(
      "#project #gitlab .loading"
    ) as HTMLElement;
    if (!navBoxEl || !loadingEl) {
      console.error("Does not exist elements: navBoxEl, loadingEl");
      return;
    }

    loadingEl.style.opacity = "1";
    navBoxEl.style.opacity = "0";

    instances.gitlabService
      .getAllRepository(pageIndex, pageSize)
      .then((page) => {
        const nextPageBtn = navBoxEl.querySelector(
          "button.next-btn"
        ) as HTMLButtonElement;
        const prevPageBtn = navBoxEl.querySelector(
          "button.previous-btn"
        ) as HTMLButtonElement;
        const pageInfoEl = navBoxEl.querySelector(".page-info");
        const gitlabProjectsEl = document.querySelector(
          "#project #gitlab .project-list"
        );

        if (navBoxEl && gitlabProjectsEl) {
          while (gitlabProjectsEl.firstChild) {
            gitlabProjectsEl.removeChild(gitlabProjectsEl.firstChild);
          }

          {
            // NAV
            if (!page.nextable) {
              nextPageBtn.disabled = true;
            } else {
              nextPageBtn.disabled = false;
              nextPageBtn.onclick = () => {
                buildGitLabProjects(page.pageNumber, pageSize);
              };
            }

            if (!page.previouseable) {
              prevPageBtn.disabled = true;
            } else {
              prevPageBtn.disabled = false;
              prevPageBtn.onclick = () => {
                buildGitLabProjects(page.pageNumber - 2, pageSize);
              };
            }
          }

          {
            // PAGE DISPLAY
            const pageNumberEl = pageInfoEl.querySelector(".pageNumber");
            const totalPagesEl = pageInfoEl.querySelector(".totalPages");
            pageNumberEl.textContent = String(page.pageNumber);
            totalPagesEl.textContent = String(page.totalPages);
          }

          // PROJECTS
          for (const project of page.content) {
            gitlabProjectsEl.appendChild(buildProjectItemEl(project));
          }
        } else {
          console.error("Does not exist enough elements of GitLab");
        }
      })
      .finally(() => {
        loadingEl.style.opacity = "0";
        navBoxEl.style.opacity = "1";
      });
  }

  function buildGitHubProjects() {
    const loadingEl = document.querySelector(
      "#project #github .loading"
    ) as HTMLElement;

    if (!loadingEl) {
      console.error("Does not exist element: loadingEl");
      return;
    }
    loadingEl.style.opacity = "0";

    instances.githubService
      .getAllRepository()
      .then((projects) => {
        const githubProjectsEl = document.querySelector(
          "#project #github .project-list"
        );

        if (githubProjectsEl) {
          for (const project of projects) {
            githubProjectsEl.appendChild(buildProjectItemEl(project));
          }
        } else {
          console.error("Does not exist github projects element");
        }
      })
      .finally(() => {
        loadingEl.style.opacity = "0";
      });
  }

  function loadData() {
    buildGitHubProjects();

    buildGitLabProjects(0, 10);
  }

  function rewriteFooter() {
    const footerEl = document.querySelector("footer");
    if (footerEl) {
      footerEl.textContent += " " + String(new Date().getFullYear());
    } else {
      console.error("Does not exist footer to rewrite");
    }
  }

  (function () {
    console.log("nmhillusion space");
    loadData();
    rewriteFooter();
  })();
}
