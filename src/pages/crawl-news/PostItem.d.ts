export interface PostItem {
  title: string;
  description: string;
  link: string;
  pubDate: string;
  source: string;
}