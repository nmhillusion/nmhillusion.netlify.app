export interface NewsServiceWorkerData {
  primaryKey: number;
  dateOfArrival: number;
  newsLink: string;
  linkToOpen: string;
}
