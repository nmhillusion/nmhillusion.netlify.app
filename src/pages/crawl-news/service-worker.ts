import { BackgroundActionType, BackgroundEvent } from "./BackgroundEvent";
import { NewsServiceWorkerData } from "./NewsServiceWorkerData";

declare const clients: any;

let timeLooper = null;
let mClient: MessageEventSource = null;
let latestTime = 0;

console.log("[sw] News Service Worker");

self.addEventListener("install", (event) => {
  (self as any).skipWaiting();

  // (event as any).waitUntil(); // caching etc
});

self.addEventListener("message", (event) => {
  // event is an ExtendableMessageEvent object
  // console.log(`The client sent me a message: ${JSON.stringify(event.data)}`);

  executionForEvent(event);
});

self.addEventListener("notificationclick", function (evt: any) {
  const notification = evt.notification;
  const data: NewsServiceWorkerData = notification.data;
  const action = evt.action;

  console.log("[sw] action of notification: ", action);

  if (action === "close") {
    console.log("close news notification");

    notification.close();
  } else if ("explore" === action) {
    clients.openWindow(data.newsLink);
  } else {
    console.log("open news notification: ", data, clients);
    // clients.openWindow("http://www.example.com");
    notification.close();

    (evt as any).waitUntil(
      clients.matchAll({ type: "window" }).then((windowClients) => {
        // Check if there is already a window/tab open with the target URL
        for (let i = 0; i < windowClients.length; i++) {
          const client = windowClients[i];
          // If so, just focus it.
          if (client.url === data.linkToOpen && "focus" in client) {
            return client.focus();
          }
        }

        // If not, then open the target URL in a new window/tab.
        if (clients.openWindow) {
          return clients.openWindow(data.linkToOpen);
        }
      })
    );
  }
});

function showNotification() {
  console.log("[sw] pick news to show");

  latestTime = new Date().getTime();
  doPostMessageToClient<number>({
    actionType: BackgroundActionType.PICK_NEWS,
    data: -1,
  });
}

function executionForEvent__initialize(
  event: MessageEvent<BackgroundEvent<number>>
) {
  const greetingMessage: BackgroundEvent<string> = {
    actionType: BackgroundActionType.INITIALIZE,
    data: "Hi client",
  };

  event.source.postMessage(greetingMessage);
  mClient = event.source;

  clearInterval(timeLooper);
  timeLooper = setInterval(showNotification, event.data.data * 1_000);
  showNotification();
}

function executionForEvent__askLatestTime(
  event: MessageEvent<BackgroundEvent<void>>
) {
  const answerLatestTimeMessage: BackgroundEvent<number> = {
    actionType: BackgroundActionType.ANSWER_LATEST_TIME,
    data: latestTime,
  };
  doPostMessageToClient(answerLatestTimeMessage);
}

function executionForEvent(event: MessageEvent<BackgroundEvent<any>>) {
  const eventData = event.data;
  if (BackgroundActionType.INITIALIZE.equals(eventData.actionType)) {
    executionForEvent__initialize(event);
  } else if (
    BackgroundActionType.ASK_LATEST_TIME.equals(eventData.actionType)
  ) {
    executionForEvent__askLatestTime(event);
  }
}
function doPostMessageToClient<T>(message: BackgroundEvent<T>) {
  // console.log("current client: ", !!mClient);

  mClient?.postMessage(message);
}
