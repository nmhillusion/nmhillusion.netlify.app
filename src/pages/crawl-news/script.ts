import { ArrayHelper } from "../../modules/helpers/array.helper";
import { ServiceWorkerHelper } from "../../modules/helpers/ServiceWorker.helper";
import { BackgroundActionType, BackgroundEvent } from "./BackgroundEvent";
import { NewsServiceWorkerData } from "./NewsServiceWorkerData";
import { PostItem } from "./PostItem";

namespace pages {
  const SERVER_HOST = "{{ n2v:SERVER_HOST }}";
  let NEWS_INTERNAL_TIME = 120;
  let newsLooper = null;
  let postItems: PostItem[] = [];
  const newsBoardEl = document.querySelector(".news-board") as HTMLDivElement;

  async function crawlFromServer(): Promise<PostItem[]> {
    return await (
      await fetch(`${SERVER_HOST}/crawl-news`, {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      })
    ).json();
  }

  function beautifulizeDescription(description: string): string {
    return description?.replace("</a>", "").replace(/<a\s+(?:.*?)>/gi, "");
  }

  function buildPostItems() {
    const newsListEl = document.querySelector(".news-list");

    if (!newsListEl) {
      console.error("Does not exist newsListEl");
    } else {
      postItems = ArrayHelper.shuffle(postItems);

      for (let idx = 0; idx < postItems.length; ++idx) {
        const item = postItems[idx];
        item.description = beautifulizeDescription(item.description);

        const newsItemEl = document.createElement("div") as HTMLDivElement;
        newsItemEl.className = `news-item news-${idx}`;

        {
          const titleEl = document.createElement("div");
          titleEl.className = "title";
          titleEl.textContent = item.title;

          const descriptionEl = document.createElement("div");
          descriptionEl.className = "description";
          descriptionEl.innerHTML = item.description;

          const newsFooter = document.createElement("div");
          newsFooter.classList.add("news-footer");
          {
            const sourceEl = document.createElement("div");
            sourceEl.className = "source";
            sourceEl.textContent = item.source;

            const linkToNews = document.createElement("a");
            linkToNews.className = "open-news";
            linkToNews.textContent = "open news";
            linkToNews.target = "_blank";
            linkToNews.href = item.link;

            const pubDateEl = document.createElement("div");
            pubDateEl.className = "public-date";
            pubDateEl.textContent = item.pubDate;

            newsFooter.appendChild(sourceEl);
            newsFooter.appendChild(linkToNews);
            newsFooter.appendChild(pubDateEl);
          }

          newsItemEl.appendChild(titleEl);
          newsItemEl.appendChild(descriptionEl);
          newsItemEl.appendChild(newsFooter);

          newsItemEl.onclick = () => {
            pickNewsToBoard({
              defaultRandomIdx: idx,
            });
          };
        }

        newsListEl.appendChild(newsItemEl);
      }

      clearInterval(newsLooper);
      newsLooper = setInterval(() => waitTimeToPickNewsToBoard(), 1_000);
      waitTimeToPickNewsToBoard();
    }
  }

  function waitTimeToPickNewsToBoard(): void {
    if (!newsBoardEl) {
      console.error("Does not exist element: .news-board");
    } else {
      newsBoardEl.style.opacity = "1";
      ServiceWorkerHelper.postMessage<void>({
        actionType: BackgroundActionType.ASK_LATEST_TIME,
        data: null,
      });
    }
  }

  function updateCountDownTime(latestTime: number) {
    const countDownEl = newsBoardEl.querySelector(
      ".count-down"
    ) as HTMLLinkElement;

    if (!countDownEl) {
      console.error("Does not exist element: .count-down");
    } else {
      const currentCount = new Date().getTime() - Number(latestTime);
      const deltaTime = Math.floor(
        (NEWS_INTERNAL_TIME * 1000 - currentCount) / 1000
      );

      countDownEl.textContent = formatTime(Math.max(0, deltaTime));

      countDownEl.onclick = function () {
        const newIntervalTime = prompt(
          "Set Interval Time: ",
          String(NEWS_INTERNAL_TIME)
        );
        if (newIntervalTime && Number.isInteger(Number(newIntervalTime))) {
          console.log("set new NEWS_INTERNAL_TIME=", newIntervalTime);

          NEWS_INTERNAL_TIME = Number(newIntervalTime);
          ServiceWorkerHelper.postMessage<number>({
            actionType: BackgroundActionType.INITIALIZE,
            data: NEWS_INTERNAL_TIME,
          });

          buildPostItems();
        }
      };
    }
  }

  function pickNewsToBoard({
    defaultRandomIdx = -1,
  }: {
    defaultRandomIdx: number;
  }) {
    console.log("[default] pick a news to show: ", defaultRandomIdx);

    const randomIdx =
      Number.isInteger(Number(defaultRandomIdx)) && -1 !== defaultRandomIdx
        ? defaultRandomIdx
        : Math.floor(Math.random() * postItems.length);
    const randomNews = postItems[randomIdx];
    console.log("[actually] pick a news to show: ", randomIdx);

    const newsIframe = newsBoardEl.querySelector(
      "iframe#board-news"
    ) as HTMLIFrameElement;
    const titleNewsBoard = newsBoardEl.querySelector(
      "a.title"
    ) as HTMLLinkElement;

    const descriptionEl = newsBoardEl.querySelector(
      ".board-description"
    ) as HTMLDivElement;

    if (!newsIframe || !titleNewsBoard || !descriptionEl) {
      console.error("Does not exist news board elements");
      return;
    }

    newsIframe.src = randomNews.link;
    titleNewsBoard.textContent = `${randomNews.title} (${randomNews.source})`;
    titleNewsBoard.href = randomNews.link;
    descriptionEl.innerHTML = randomNews.description;

    showNotification(randomNews);

    document
      .querySelectorAll(".news-item")
      .forEach((el) => el.classList.remove("selected"));
    document.querySelector(".news-" + randomIdx)?.classList.add("selected");
  }

  function formatTime(timeNumber: number): string {
    const mm = Math.floor(timeNumber / 60);
    timeNumber -= mm * 60;

    function fill2chars(x: number) {
      if (x < 10) {
        return "0" + x;
      } else {
        return String(x);
      }
    }

    return `${fill2chars(mm)}:${fill2chars(timeNumber)}`;
  }

  async function registerNotificationService() {
    let granted = false;

    if (Notification.permission === "granted") {
      granted = true;
    } else if (Notification.permission !== "denied") {
      let permission = await Notification.requestPermission();
      granted = permission === "granted" ? true : false;
    }

    if (!granted) {
      console.error("You blocked the notifications");
    } else {
      await registerServiceWorker();
    }
  }

  function registerServiceWorker() {
    return ServiceWorkerHelper.registerServiceWorker(
      "/pages/crawl-news/service-worker.js",
      "/pages/crawl-news/"
    );
  }

  function executionEventFromServiceWorker(event: MessageEvent<any>) {
    const eventData = event.data as BackgroundEvent<any>;

    if (BackgroundActionType.INITIALIZE.equals(eventData.actionType)) {
      console.log("Handshake successful: ", eventData.data);
    } else if (BackgroundActionType.PICK_NEWS.equals(eventData.actionType)) {
      pickNewsToBoard({
        defaultRandomIdx: -1,
      });
    } else if (
      BackgroundActionType.ANSWER_LATEST_TIME.equals(eventData.actionType)
    ) {
      updateCountDownTime(eventData.data);
    }
  }

  (async function main() {
    console.log("working crawl news ::");

    await registerNotificationService();

    postItems = await crawlFromServer();
    console.log({ postItems });

    buildPostItems();

    const loadingContainerEl = document.querySelector(".loading-container");
    if (loadingContainerEl) {
      loadingContainerEl.parentNode.removeChild(loadingContainerEl);
    }

    ServiceWorkerHelper.listenOnMessage((event) => {
      // console.log("listen on message of service worker: ", event);
      executionEventFromServiceWorker(event);
    });

    ServiceWorkerHelper.postMessage<number>({
      actionType: BackgroundActionType.INITIALIZE,
      data: NEWS_INTERNAL_TIME,
    });
  })();
}

function showNotification(randomNews: any) {
  if (Notification.permission !== "granted") {
    return;
  }

  const title = `${randomNews.title} (${randomNews.source})`;
  const body = randomNews.description?.replace(/\<.*?\>/g, "");
  let image = null;

  const matched = randomNews.description?.match(
    /<img\s(?:.*?)src=(?:'|"|\\'|\\")(.*?)(?:'|"|\\'|\\")(?:.*?)>/
  );

  if (matched) {
    image = matched[1];
  }

  navigator.serviceWorker.getRegistration().then(function (reg) {
    reg.showNotification(title, {
      body,
      image,
      vibrate: [100, 50, 100],
      data: {
        primaryKey: 1,
        dateOfArrival: Date.now(),
        newsLink: randomNews.link,
        linkToOpen: location.href,
      } as NewsServiceWorkerData,
      actions: [
        {
          action: "explore",
          title: "Explore News",
        },
        {
          action: "reopen",
          title: "ReOpen",
        },
        {
          action: "close",
          title: "Close",
        },
      ],
    });
    // reg.showNotification(JSON.stringify(randomNews));
  });
}
