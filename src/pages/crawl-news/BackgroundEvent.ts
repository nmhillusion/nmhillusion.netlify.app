export interface BackgroundEvent<T> {
  actionType: BackgroundActionType;
  data: T;
}

export class BackgroundActionType {
  static INITIALIZE = new BackgroundActionType("INITIALIZE");
  static PICK_NEWS = new BackgroundActionType("PICK_NEWS");
  static TERMINATE = new BackgroundActionType("TERMINATE");
  static ASK_LATEST_TIME = new BackgroundActionType("ASK_LATEST_TIME");
  static ANSWER_LATEST_TIME = new BackgroundActionType("ANSWER_LATEST_TIME");
  static SET_INTERVAL_TIME = new BackgroundActionType("SET_INTERVAL_TIME");

  constructor(private value: string) {}

  equals(other: BackgroundActionType) {
    return this.value === other.value;
  }
}
