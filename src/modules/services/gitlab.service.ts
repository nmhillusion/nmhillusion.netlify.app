import { Pageable } from "../models/pageable";
import { Project } from "../models/project";
import { HttpHelper } from "../helpers/http.helper";

export class GitLabService {
  private BASE_API = "https://gitlab.com/api/v4";
  private httpHelper: HttpHelper;

  constructor(private username: string, private accessToken: string) {
    this.httpHelper = new HttpHelper();
  }

  private userId(): Promise<number> {
    function getLocalStorageKey(): string {
      return `main.project.gitlab.userId`;
    }

    return new Promise<number>((resolve, reject) => {
      const cached = localStorage.getItem(getLocalStorageKey());
      if (cached) {
        resolve(Number(cached));
      } else {
        this.httpHelper
          .xBasicFetch({
            method: this.httpHelper.REQUEST_METHODS.GET,
            url: this.BASE_API + `/user`,
            responseType: this.httpHelper.RESPONSE_TYPES.JSON,
            headers: this.buildAuthenticationAndDefaultHeader(),
          })
          .then((response) => {
            const userId = response.data["id"];

            localStorage.setItem(getLocalStorageKey(), userId);

            resolve(userId);
          })
          .catch(reject);
      }
    });
  }

  private buildAuthenticationAndDefaultHeader() {
    const tokenKey = this.accessToken;
    return {
      "PRIVATE-TOKEN": `${tokenKey}`,
    };
  }

  getAllRepository(
    pageIndex: number,
    pageSize: number
  ): Promise<Pageable<Project>> {
    return new Promise<Pageable<Project>>(async (resolve, reject) => {
      this.httpHelper
        .xBasicFetch({
          method: this.httpHelper.REQUEST_METHODS.GET,
          url:
            this.BASE_API +
            `/users/${await this.userId()}/projects?page=${
              pageIndex + 1
            }&per_page=${pageSize}&order_by=id&sort=desc`,
          responseType: this.httpHelper.RESPONSE_TYPES.JSON,
          headers: this.buildAuthenticationAndDefaultHeader(),
        })
        .then((response) => {
          // console.log("GitLab data: ", response);
          const { data, headers } = response;
          const resultContent: Project[] = [];
          const nextable = !!headers.get("X-Next-Page");
          const previouseable = !!headers.get("X-Prev-Page");
          const totalElements = Number(headers.get("X-Total"));
          const totalPages = Number(headers.get("X-Total-Pages"));
          const pageNumber = Number(headers.get("X-Page"));

          for (const iProj of data) {
            resultContent.push({
              name: iProj["name"],
              description: iProj["description"],
              url: iProj["web_url"],
              isPublic: "public" == iProj["visibility"],
              languages: this.getLanguegesOfProject(iProj["id"]),
            });
          }

          resolve({
            content: resultContent,
            nextable,
            previouseable,
            totalElements,
            totalPages,
            pageNumber,
          });
        })
        .catch((error) => {
          console.error("GitLab Error: ", error);
          reject(error);
        });
    });
  }

  private getLanguegesOfProject(projectId: string): Promise<string[]> {
    function getLocalStorageKey(projectId: string): string {
      return `main.project.gitlab.languages.${projectId}`;
    }

    return new Promise<string[]>((resolve, reject) => {
      const cached = sessionStorage.getItem(getLocalStorageKey(projectId));
      if (cached) {
        resolve(JSON.parse(cached));
      } else {
        this.httpHelper
          .xBasicFetch({
            method: this.httpHelper.REQUEST_METHODS.GET,
            url: this.BASE_API + `/projects/${projectId}/languages`,
            responseType: this.httpHelper.RESPONSE_TYPES.JSON,
            headers: this.buildAuthenticationAndDefaultHeader(),
          })
          .then((response) => {
            const data = response.data;
            const languages = Object.keys(data);

            sessionStorage.setItem(
              getLocalStorageKey(projectId),
              JSON.stringify(languages)
            );
            resolve(languages);
          })
          .catch((error) => {
            console.error(error);
            resolve([]);
          });
      }
    });
  }
}
