import { Project } from "../models/project";
import { HttpHelper } from "../helpers/http.helper";

export class GitHubService {
  private BASE_API = "https://api.github.com";
  private httpHelper: HttpHelper;

  constructor(private username: string, private accessToken: string) {
    this.httpHelper = new HttpHelper();
  }

  private buildAuthenticationAndDefaultHeader() {
    const rawAuthentication = `${this.username}:${this.accessToken}`;
    const authentication = btoa(rawAuthentication);

    return {
      Accept: "application/vnd.github.v3+json",
      Authorization: `Basic ${authentication}`,
    };
  }

  getAllRepository(): Promise<Project[]> {
    return new Promise<Project[]>((resolve, reject) => {
      this.httpHelper
        .xBasicFetch({
          method: this.httpHelper.REQUEST_METHODS.GET,
          url: this.BASE_API + "/user/repos",
          responseType: this.httpHelper.RESPONSE_TYPES.JSON,
          headers: this.buildAuthenticationAndDefaultHeader(),
        })
        .then(async (response) => {
          // console.log("GitHub data: ", response);

          const data = response.data;
          const result: Project[] = [];

          for (const iProj of data) {
            const languages = this.getLanguegesOfProject(
              iProj["languages_url"]
            );

            result.push({
              name: iProj["full_name"],
              description: iProj["description"],
              isPublic: "public" == iProj["visibility"],
              url: iProj["html_url"],
              languages,
            });
          }

          resolve(result);
        })
        .catch((error) => {
          console.error("GitHub Error: ", error);
          reject(error);
        });
    });
  }

  private getLanguegesOfProject(languagesUrl: string): Promise<string[]> {
    function getLocalStorageKey(languagesUrl: string): string {
      return `main.project.github.${languagesUrl}`;
    }

    return new Promise<string[]>((resolve, reject) => {
      const cached = sessionStorage.getItem(getLocalStorageKey(languagesUrl));
      if (cached) {
        resolve(JSON.parse(cached));
      } else {
        this.httpHelper
          .xBasicFetch({
            method: this.httpHelper.REQUEST_METHODS.GET,
            url: languagesUrl,
            responseType: this.httpHelper.RESPONSE_TYPES.JSON,
            headers: this.buildAuthenticationAndDefaultHeader(),
          })
          .then((response) => {
            const data = response.data;
            const languages = Object.keys(data);

            sessionStorage.setItem(
              getLocalStorageKey(languagesUrl),
              JSON.stringify(languages)
            );
            resolve(languages);
          })
          .catch((error) => {
            console.error(error);
            resolve([]);
          });
      }
    });
  }
}
