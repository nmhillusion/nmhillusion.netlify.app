export interface Project {
  name: string;
  description: string;
  url: string;
  languages: Promise<string[]>;
  isPublic: boolean;
}