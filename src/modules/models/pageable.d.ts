export interface Pageable<T> {
  content: T[];
  nextable: boolean;
  previouseable: boolean;
  totalPages: number;
  totalElements: number;
  pageNumber: number;
}
