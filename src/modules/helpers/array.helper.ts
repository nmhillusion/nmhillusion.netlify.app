export class ArrayHelper {
  static shuffle(items: any[]) {
    if (!items) {
      return items;
    } else {
      const randomIdxes = [];
      while (items.length > randomIdxes.length) {
        const randomIdx = Math.floor(Math.random() * items.length);

        if (randomIdx < items.length && !randomIdxes.includes(randomIdx)) {
          randomIdxes.push(randomIdx);
        }
      }

      const newItems = [];
      for (let idx = 0; idx < items.length; ++idx) {
        newItems[idx] = items[randomIdxes[idx]];
      }
      return newItems;
    }
  }
}
