export class HttpResponseType {
  constructor(private readonly value: number) {}

  equals(another: HttpResponseType): boolean {
    return this.value === another.value;
  }
}

export class RequestMethod {
  constructor(public readonly value: string) {}
}

export class HttpHelper {
  RESPONSE_TYPES = {
    TEXT: new HttpResponseType(1),
    JSON: new HttpResponseType(2),
    BLOB: new HttpResponseType(3),
    ARRAY_BUFFER: new HttpResponseType(4),
  };

  REQUEST_METHODS = {
    GET: new RequestMethod("GET"),
    POST: new RequestMethod("POST"),
    PUT: new RequestMethod("PUT"),
    DELETE: new RequestMethod("DELETE"),
    HEAD: new RequestMethod("HEAD"),
  };

  xBasicFetch({
    url,
    method,
    params,
    body,
    headers,
    responseType = this.RESPONSE_TYPES.TEXT,
  }: {
    url: string;
    method: RequestMethod;
    params?: { key: string; value: any }[];
    body?: { key: string; value: any }[] | Object;
    headers?: HeadersInit;
    responseType?: HttpResponseType;
  }) {
    return new Promise<{ data: any; headers: Headers }>(
      (resolve, reject) => {
        let combinedUrl = url;

        if (params && 0 < params.length) {
          const combinedParams = params
            .map((entry) => `${entry.key}=${entry.value}`)
            .join("&");
          if (url.includes("?")) {
            combinedUrl += "&" + combinedParams;
          } else {
            combinedUrl += "?" + combinedParams;
          }
        }

        fetch(combinedUrl, {
          method: method.value,
          headers,
          body: null == body ? null : JSON.stringify(body),
          redirect: "follow",
        })
          .then(async (r) => {
            if (!r.ok) {
              throw new Error(await r.text());
            } else {
              let data: any = null;

              if (this.RESPONSE_TYPES.TEXT.equals(responseType)) {
                data = await r.text();
              } else if (this.RESPONSE_TYPES.JSON.equals(responseType)) {
                data = await r.json();
              } else if (this.RESPONSE_TYPES.BLOB.equals(responseType)) {
                data = await r.blob();
              } else if (
                this.RESPONSE_TYPES.ARRAY_BUFFER.equals(responseType)
              ) {
                data = await r.arrayBuffer();
              }

              resolve({
                data,
                headers: r.headers,
              });
            }
          })
          .catch(reject);
      }
    );
  }
}
