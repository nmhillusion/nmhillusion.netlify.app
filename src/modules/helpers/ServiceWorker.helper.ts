import { BackgroundEvent } from "../../pages/crawl-news/BackgroundEvent";

export class ServiceWorkerHelper {
  static async registerServiceWorker(scriptPath: string, scope: string) {
    if ("serviceWorker" in navigator) {
      try {
        const registration = await navigator.serviceWorker.register(
          scriptPath,
          {
            scope,
            type: "module",
          }
        );
        if (registration.installing) {
          console.log("Service worker installing");
        } else if (registration.waiting) {
          console.log("Service worker installed");
        } else if (registration.active) {
          console.log("Service worker active");
        }
      } catch (error) {
        console.error(`Registration failed with ${error}`);
      }
    }
  }

  static listenOnMessage(callback: (event: MessageEvent<any>) => void) {
    navigator.serviceWorker?.addEventListener("message", callback);
  }

  static postMessage<T>(data: BackgroundEvent<T>) {
    navigator.serviceWorker?.ready.then((registration) => {
      registration.active.postMessage(data);
    });
  }
}
