import { BullEngine } from "@nmhillusion/n2ngin-bull-engine";
import * as fs from "fs";
import { LogFactory } from "@nmhillusion/n2log4web";
const [_, __, mode] = process.argv;
const logger = LogFactory.getNodeLog("render");

(function (mode) {
  const tsConfigContent = fs
    .readFileSync(process.cwd() + "/tsconfig.json")
    .toString();

  console.log({ tsConfigContent });

  const tsConfig = JSON.parse(tsConfigContent);
  const MODE_PRODUCTION = "PROD" === mode;

  logger.info({ MODE_PRODUCTION, tsConfig });

  const envFile = !MODE_PRODUCTION
    ? process.cwd() + "/environments/dev.json"
    : process.cwd() + "/environments/prod.json";

  new BullEngine()
    .setVariableFilePathToInject(envFile)
    .config({
      copyResource: {
        enabled: true,
      },
      outDir: process.cwd() + "/dist",
      pug: {
        enabled: true,
        config: {
          pretty: false,
        },
      },
      rootDir: process.cwd() + "/src",
      scss: {
        enabled: true,
        config: {
          outputStyle: "compressed",
          sourceMap: false,
        },
      },
      typescript: {
        enabled: true,
        config: tsConfig["compilerOptions"],
      },
      rewriteJavascript: {
        enabled: true,
        config: {
          compress: MODE_PRODUCTION,
          rewriteImport: true,
        },
      },
      watch: {
        enabled: !MODE_PRODUCTION,
        handleChangeEvent: true,
        handleRenameEvent: true,
        minIntervalInMs: 1_500,
      },
    })
    .render();
})(mode);
